'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Character extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Character.belongsTo(models.Movie);
      Character.belongsTo(models.Cast);
    }
  };
  Character.init({
    movie_id: DataTypes.INTEGER,
    cast_id: DataTypes.INTEGER,
    character_name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Character',
  });
  return Character;
};