'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User.hasOne(model.Role_user, {foreignKey: 'role_id'});
      User.hasMany(models.Review, {foreignKey: 'user_id'});
    }
  };
  User.init({
    username: DataTypes.STRING,
    name: DataTypes.STRING,
    role_id: DataTypes.INTEGER,
    profile_pic: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};