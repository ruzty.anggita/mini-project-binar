'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Role_user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Role_user.belongsTo(models.User);
    }
  };
  Role_user.init({
    name_role: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Role_user',
  });
  return Role_user;
};