'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Movie extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Movie.hasOne(models.Movie_info, {foreignKey: 'movie_id'});
      Movie.hasMany(models.Review, {foreignKey: 'movie_id'});
      Movie.hasMany(models.Character, {foreignKey: 'movie_id'});
      Movie.hasMany(models.Movie_category, {foreignKey: 'movie_id'});
    }
  };
  Movie.init({
    title: DataTypes.STRING,
    synopsis: DataTypes.STRING,
    trailer: DataTypes.STRING,
    poster: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Movie',
  });
  return Movie;
};