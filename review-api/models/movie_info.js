'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Movie_info extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Movie_info.belongsTo(models.Movie);
    }
  };
  Movie_info.init({
    movie_id: DataTypes.INTEGER,
    release_date: DataTypes.DATEONLY,
    director: DataTypes.STRING,
    feature_song: DataTypes.STRING,
    budget: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Movie_info',
  });
  return Movie_info;
};