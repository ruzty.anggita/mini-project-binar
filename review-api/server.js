const express = require('express');
const bodyParser = require('body-parser');
const { Movie } = require('./models/movie');

// const authRoute = require('./routes/authRoute');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

// ROUTER
// app.use(authRoute);
app.get('/', async(req, res) => {

})

const port = process.env.PORT || 3005 ;
app.listen(3005, () => {
    console.log(`Server is running on ${port}`);
});
