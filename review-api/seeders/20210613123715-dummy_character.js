'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert(
      "Characters",
      [
        {
          movie_id: 1,
          cast_id: 1,
          character_name: "Rose",
          createdAt: new Date(),
          updatedAt: new Date()      
        },
        {
          movie_id: 1,
          cast_id: 2,
          character_name: "Jack",
          createdAt: new Date(),
          updatedAt: new Date()      
        },
        {
          movie_id: 2,
          cast_id: 3,
          character_name: "Nobita",
          createdAt: new Date(),
          updatedAt: new Date()      
        },
        {
          movie_id: 2,
          cast_id: 4,
          character_name: "Doraemon",
          createdAt: new Date(),
          updatedAt: new Date()      
        },
        {
          movie_id: 3,
          cast_id: 5,
          character_name: "John",
          createdAt: new Date(),
          updatedAt: new Date()      
        },
        {
          movie_id: 3,
          cast_id: 6,
          character_name: "Jane",
          createdAt: new Date(),
          updatedAt: new Date()      
        },
        {
          movie_id: 4,
          cast_id: 7,
          character_name: "Nobita",
          createdAt: new Date(),
          updatedAt: new Date()      
        },
        {
          movie_id: 4,
          cast_id: 8,
          character_name: "Doraemon",
          createdAt: new Date(),
          updatedAt: new Date()      
        }
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
