'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert(
      "Users",
      [
        {
          username: "ahmadputra",
          name: "Ahmad Putra",
          role_id: 1 ,
          profile_pic: "profile_pic",
          password: "ahmad123",
          email: "ahmadputra@gmail.com",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "putrilili",
          name: "Putri Lili",
          role_id: 1 ,
          profile_pic: "profile_pic",
          password: "putri123",
          email: "putrilili@gmail.com",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "admin123",
          name: "Admin",
          role_id: 2 ,
          profile_pic: "profile_pic", 
          password: "adminadmin",
          email: "admin@gmail.com",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "rosalina",
          name: "Rosa Lina",
          role_id: 1 ,
          profile_pic: "profile_pic",
          password: "rosa123",
          email: "rosalina@gmail.com",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
