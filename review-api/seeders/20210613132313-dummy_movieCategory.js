'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert(
      "Movie_categories",
      [
        {
          category_id: 1,
          movie_id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          category_id: 2,
          movie_id: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          category_id: 3,
          movie_id: 3,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          category_id: 4,
          movie_id: 4,
          createdAt: new Date(),
          updatedAt: new Date(),
        },

      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
