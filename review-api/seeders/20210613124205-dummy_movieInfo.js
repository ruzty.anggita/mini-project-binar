'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert(
      "Movie_infos",
      [
        {
          movie_id: 1,
          release_date: "1998-01-05",
          director: "James Cameron",
          feature_song: "My Heart Will Go On",
          budget:255666999,
          createdAt: new Date(),
          updatedAt: new Date()   
        },
        {
          movie_id: 2,
          release_date: "2014-12-18",
          director: " Takashi Yamazaki",
          feature_song: "Himawari no Yakusoku",
          budget:34999999,
          createdAt: new Date(),
          updatedAt: new Date()   
        },
        {
          movie_id: 3,
          release_date: "1999-07-03",
          director: "Lana Wachowski",
          feature_song: "Rock is Dead",
          budget:63123999,
          createdAt: new Date(),
          updatedAt: new Date()   
        },
        {
          movie_id: 4,
          release_date: "2014-11-19",
          director: " Raditya Dika",
          feature_song: "Anugerah Terindah yang Pernah Kumiliki",
          budget:712999,
          createdAt: new Date(),
          updatedAt: new Date()   
        }
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
