'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert(
      "Categories",
      [
        {
          category_name: "Comedy",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          category_name: "Drama",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          category_name: "Romance",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          category_name: "Action",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          category_name: "Sci-fi",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          category_name: "Thriller",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          category_name: "Cartoon",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          category_name: "Horror",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          category_name: "Fantasy",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          category_name: "Mistery",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
