'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert(
      "Casts",
      [
        {
          name_cast: "Kate Winslet" ,
          profile_cast: "ini-foto-katewinslet" ,
          gender: "Female",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name_cast: "Leonardo Dicaprio" ,
          profile_cast: "ini-foto-leonardo" ,
          gender: "Male",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name_cast: "Pemeran Nobita" ,
          profile_cast: "ini-foto-nobita" ,
          gender: "Male",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name_cast: "Pemeran Doraemon" ,
          profile_cast: "ini-foto-doraemon" ,
          gender: "Male",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name_cast: "Keanu Reeves" ,
          profile_cast: "ini-foto-keanu" ,
          gender: "Male",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name_cast: "Winona Ryder" ,
          profile_cast: "ini-foto-winona" ,
          gender: "Female",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name_cast: "Raditya Dika" ,
          profile_cast: "ini-foto-raditya" ,
          gender: "Male",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name_cast: "Nikita Willy" ,
          profile_cast: "ini-foto-nikita" ,
          gender: "Female",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name_cast: "Dian Sastro" ,
          profile_cast: "ini-foto-dsastro" ,
          gender: "Female",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
